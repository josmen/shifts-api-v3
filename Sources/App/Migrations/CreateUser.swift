import Fluent
import Vapor

struct CreateUser: AsyncMigration {
    func prepare(on database: Database) async throws {
        try await database.schema(User.schema)
            .id()
            .field(User.FieldKeys.username, .string, .required)
            .field(User.FieldKeys.password, .string, .required)
            .field(User.FieldKeys.isAdmin, .bool, .required)
            .unique(on: User.FieldKeys.username)
            .create()
    }

    func revert(on database: Database) async throws {
        try await database.schema(User.schema).delete()
    }
}

struct CreateAdmin: AsyncMigration {
    func prepare(on database: Database) async throws {
        let user = User(username: "admin", password: try Bcrypt.hash("password"), isAdmin: true)
        try await user.create(on: database)
    }
    
    func revert(on database: Database) async throws {
        try await User.query(on: database).filter(\.$username == "admin").delete()
    }
}
