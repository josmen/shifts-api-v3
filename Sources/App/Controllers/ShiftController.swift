import Fluent
import ShiftsAppSharedDTO
import Vapor

struct ShiftController: RouteCollection {
    func boot(routes: RoutesBuilder) throws {
        let publicShiftRoutes = routes.grouped("api", "shifts")
        publicShiftRoutes.get(use: getAllHandler)
        publicShiftRoutes.get(":shiftID", use: getHandler)
        publicShiftRoutes.get("actual", use: getActualShiftsByCategoryAndLocationHandler)
        publicShiftRoutes.get(":shiftID", "group", use: getGroupHandler)
        
        let privateShiftRoutes = publicShiftRoutes.grouped(JWTAuthenticator())
        privateShiftRoutes.post(use: createHandler)
        privateShiftRoutes.put(":shiftID", use: updateHandler)
        privateShiftRoutes.delete(":shiftID", use: deleteHandler)
    }
    
    // MARK: - Public routes
    // GET /api/shifts
    func getAllHandler(_ req: Request) async throws -> [ShiftResponseDTO] {
        try await Shift
            .query(on: req.db)
            .all()
            .compactMap(ShiftResponseDTO.init)
    }
    
    // GET /api/shifts/{shiftID}
    func getHandler(_ req: Request) async throws -> ShiftResponseDTO {
        guard let shift = try await Shift.find(req.parameters.get("shiftID"), on: req.db) else {
            throw Abort(.notFound)
        }
        
        guard let shiftResponseDTO = ShiftResponseDTO(shift) else {
            throw Abort(.internalServerError)
        }
        
        return shiftResponseDTO
    }
    
    // GET /api/shifts/actual?category=maquinista&location=benidorm
    func getActualShiftsByCategoryAndLocationHandler(_ req: Request) async throws -> [ShiftResponseDTO] {
        guard let category = req.query[String.self, at: "category"],
              let location = req.query[String.self, at: "location"] else {
            throw Abort(.badRequest)
        }
        
        guard let group = try await ShiftsGroup
            .query(on: req.db)
            .filter(\.$category == category)
            .filter(\.$location == location)
            .first()
        else {
            throw Abort(.notFound)
        }
        
        return try await group.$shifts
            .query(on: req.db)
            .all()
            .compactMap(ShiftResponseDTO.init)
    }
    
    // GET /api/shifts/{shiftID}/group
    func getGroupHandler(_ req: Request) async throws -> ShiftsGroupResponseDTO {
        guard let shift = try await Shift.find(req.parameters.get("shiftID"), on: req.db),
              let group = try await shift.$shiftsGroup.query(on: req.db).first()
        else {
            throw Abort(.notFound)
        }
        
        guard let shiftsGroupResponseDTO = ShiftsGroupResponseDTO(group) else {
            throw Abort(.internalServerError)
        }
        
        return shiftsGroupResponseDTO
    }
    
    // MARK: - Protected routes
    // POST /api/shifts
    func createHandler(_ req: Request) async throws -> ShiftResponseDTO {
        let data = try req.content.decode(ShiftRequestDTO.self)
        
        guard let _ = try await ShiftsGroup.find(data.shiftsGroupID, on: req.db) else {
            throw Abort(.badRequest, reason: "Shifts Group not found.")
        }
        
        let shift = Shift(
            name: data.name,
            start: data.start,
            end: data.end,
            saturation: data.saturation ?? 0,
            shiftsGroupID: data.shiftsGroupID)
        try await shift.save(on: req.db)
        
        guard let shiftResponseDTO = ShiftResponseDTO(shift) else {
            throw Abort(.internalServerError)
        }
        
        return shiftResponseDTO
    }
    
    // PUT /api/shifts/{shiftID}
    func updateHandler(_ req: Request) async throws -> ShiftResponseDTO {
        let updatedShift = try req.content.decode(ShiftRequestDTO.self)
        
        guard let shift = try await Shift.find(req.parameters.get("shiftID"), on: req.db) else {
            throw Abort(.notFound)
        }
        
        guard let _ = try await ShiftsGroup.find(updatedShift.shiftsGroupID, on: req.db) else {
            throw Abort(.badRequest, reason: "Shifts Group not found.")
        }
        
        shift.name = updatedShift.name
        shift.start = updatedShift.start
        shift.end = updatedShift.end
        shift.saturation = updatedShift.saturation ?? 0
        shift.$shiftsGroup.id = updatedShift.shiftsGroupID
        
        try await shift.save(on: req.db)
        
        guard let shiftResponseDTO = ShiftResponseDTO(shift) else {
            throw Abort(.internalServerError)
        }
        
        return shiftResponseDTO
    }
    
    // DELETE /api/shifts/{shiftID}
    func deleteHandler(_ req: Request) async throws -> HTTPStatus {
        guard let shift = try await Shift.find(req.parameters.get("shiftID"), on: req.db) else {
            throw Abort(.notFound)
        }
        try await shift.delete(on: req.db)
        return .ok
    }
}
