import ShiftsAppSharedDTO
import Vapor

extension ShiftsGroupResponseDTO: Content {
    init?(_ shiftsGroup: ShiftsGroup) {
        guard let id = shiftsGroup.id else { return nil }
        let shifts = shiftsGroup.shifts.compactMap(ShiftResponseDTO.init)
        self.init(id: id, validFrom: shiftsGroup.validFrom, category: shiftsGroup.category, location: shiftsGroup.location, shifts: shifts)
    }
}
