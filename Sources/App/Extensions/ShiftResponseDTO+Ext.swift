import ShiftsAppSharedDTO
import Vapor

extension ShiftResponseDTO: Content {
    init?(_ shift: Shift) {
        guard let id = shift.id else { return nil }
        
        self.init(id: id, name: shift.name, start: shift.start, end: shift.end, saturation: shift.saturation)
    }
}
