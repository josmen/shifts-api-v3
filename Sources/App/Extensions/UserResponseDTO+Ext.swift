import ShiftsAppSharedDTO
import Vapor

extension UserResponseDTO: Content {
    init?(_ user: User) {
        guard let id = user.id else { return nil }
        
        self.init(id: id, username: user.username, isAdmin: user.isAdmin)
    }
}
