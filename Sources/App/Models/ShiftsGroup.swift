import Fluent
import Vapor

final class ShiftsGroup: Model, Content {
    static var schema: String = "shifts_groups"
    
    struct FieldKeys {
        static var validFrom: FieldKey { "valid_from" }
        static var category: FieldKey { "category" }
        static var location: FieldKey { "location" }
    }
    
    @ID
    var id: UUID?
    
    @Field(key: FieldKeys.validFrom)
    var validFrom: Date
    
    @Field(key: FieldKeys.category)
    var category: String
    
    @Field(key: FieldKeys.location)
    var location: String
    
    @Children(for: \Shift.$shiftsGroup)
    var shifts: [Shift]
    
    init() { }
    
    init(id: UUID? = nil, validFrom: Date, category: String, location: String) {
        self.id = id
        self.validFrom = validFrom
        self.category = category
        self.location = location
    }
}
