import Fluent
import Vapor

final class Shift: Model, Content {
    static var schema: String = "shifts"
    
    struct FieldKeys {
        static var name: FieldKey { "name" }
        static var start: FieldKey { "start" }
        static var end: FieldKey { "end" }
        static var saturation: FieldKey { "saturation" }
        static var shiftsGroup: FieldKey { "group_id" }
    }
    
    @ID
    var id: UUID?
    
    @Field(key: FieldKeys.name)
    var name: String
    
    @Field(key: FieldKeys.start)
    var start: String
    
    @Field(key: FieldKeys.end)
    var end: String
    
    @Field(key: FieldKeys.saturation)
    var saturation: Double
    
    @Parent(key: FieldKeys.shiftsGroup)
    var shiftsGroup: ShiftsGroup
    
    init() { }
    
    init(id: UUID? = nil, name: String, start: String, end: String, saturation: Double, shiftsGroupID: ShiftsGroup.IDValue) {
        self.id = id
        self.name = name
        self.start = start
        self.end = end
        self.saturation = saturation
        self.$shiftsGroup.id = shiftsGroupID
    }
}
